const api = 'https://api.themoviedb.org/3/search/movie?api_key=25146f0c013c09c6a5b7492f70ea7581&language=en-US&query=';
const img = 'https://image.tmdb.org/t/p/w500';

const actordetail = 'https://api.themoviedb.org/3/person/';
function searchMovies(e)
{   
    searchMoviesByName(e);
    searchMoviesByActorName(e);
}

async function searchMoviesByName(e)
{
    e.preventDefault();
    const strSearch = $('form input').val();
 
    const req = `${api}${strSearch}`;
    loading();

    const response = await fetch(req);
    const object = await response.json();

    fillMovies(object, 'movie');
}



const filmpopular='https://api.themoviedb.org/3/movie/popular?api_key=25146f0c013c09c6a5b7492f70ea7581&language=en-US&page=';

async function HomePage()
{
    const i=1;
    const req = `${filmpopular}${i}`;

    const response = await fetch(req);
    const object = await response.json();

    fillMovies(object, 'movie');
}
function fillMovies(text, searchby)
{   
    if (searchby == 'movie')
    {
        $('#main').empty();
        var i=0;
        while (text.results[i]) 
        {
            const m = text.results[i];
            $('#main').append(`
            <div class="col-md-4 col-md-offset-1">
                <div class="card" >
                <img onclick = "loadDetail(${m.id})" src="${img}${m.poster_path}" class="card-img-top" alt="No Image">
                    <div class="card-body">
                        <h4 class="card-title">${m.original_title}<br><br>Vote rate:${m.vote_average}</h4>
                        <p class="card-text"><u>Release Day: ${m.release_date}</u></p>
                        <p class="card-text"><i>Over view: ${m.overview}</i></p>
                    </div>
                </div>
            </div>
        `);
        i=i+1;
        }
    }
    else
    {
        var j=0;
        while(text.results[j])
        {
            var i=0;
            while (text.results[j][i]) 
            {
                const m = text.results[j][i];
                $('#main').append(`
                <div class="col-md-4 col-md-offset-1">
                    <div class="card" >
                    <img onclick ="loadDetail(${m.id})" src="${img}${m.poster_path}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">${m.original_title}<br><br>${m.vote_average}</h5>
                            <p class="card-text">${m.overview}</p>
                        </div>
                    </div>
                </div>
                `);
                i=i+1;
            }
            j++;
        }      
    }
}

function loading()
{
    $('#main').empty();
    $('#main').append(`
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
}

const detail = 'https://api.themoviedb.org/3/movie/';
async function loadDetail(m)
{
    const req = `${detail}${m}?api_key=25146f0c013c09c6a5b7492f70ea7581`;
    loading();

    const response = await fetch(req);
    const object = await response.json();
    
    
    $('#main').empty();
    $('#main').append(`
    <div class="media">
    <img src="${img}${object.backdrop_path}" class="align-self-center mr-3" alt="temp">
        <div class="media-body">
            <h5 style = "font-size: 200%" class="mt-0">${object.original_title}</h5>
            <p id = "genre" ><b>Release Day:</b> ${object.release_date}
            <br><b>Production Company:</b> ${object.production_companies[1].name}
            <br><b>Genre :</b>
            </p>
            <p id = "actor"><b>Actor:</b> </p>
            <p class="mb-0"><b>Overview:</b> ${object.overview}</p>

        </div>
    </div>
    `);
   
    loadActor(m);
    loadGenre(object);
    loadReview(m);
}

function loadGenre(object)
{
    var i=0;
    while (object.genres[i])
    {    
        const temp = object.genres[i];
        $('#genre').append(`
        <a>${temp.name}, </a>
        `);
        i++;
    }
}


async function loadActor(id)
{
    const req = `${detail}${id}/credits?api_key=25146f0c013c09c6a5b7492f70ea7581`

    const response = await fetch(req);
    const object = await response.json();

    var i=0;
    while (object.cast[i])
    {    
        const temp = object.cast[i];
        $('#actor').append(`
        <a onclick = "detailActor(${temp.id})"><b style = "color:blue">${temp.name}</b>, </a>
        `);
        i++;
    }

}

async function loadReview(m)
{
    const req = `${detail}${m}/reviews?api_key=25146f0c013c09c6a5b7492f70ea7581`

    const response = await fetch(req);
    const object = await response.json();

    $('#main').append(`
    <div class="row">
        <div class="col">
            <div id = "review">
                <div id = "review">
                    <h5 class="card-title"><br><br>REVIEW<br></h5>      
                </div>
            </div>
        </div>
    </div>
    
    `);
    
    var i=0;
    while (object.results[i])
    {    
        const temp = object.results[i];
        $('#review').append(`
            <h6 class="card-subtitle mb-2 text-muted"><br>People Comment: ${temp.author}</h6>
            <p class="card-text">${temp.content}</p>
        `);
        i++;
    }
}

async function detailActor(id)
{
    const req = `${actordetail}${id}?api_key=25146f0c013c09c6a5b7492f70ea7581&language=en-US`

    const response = await fetch(req);
    const object = await response.json();

    $('#main').empty();
    $('#main').append(`
    <div class="media">
    <img src="${img}${object.profile_path}" class="align-self-center mr-3" alt="temp">
        <div id = "list" class="media-body">
            <h5 style = "font-size: 200%" class="mt-0">${object.name}</h5>
            <p class="mb-0"><b>Tieu Su:</b> ${object.biography}</p>
        </div>
    </div>
    `);

}

